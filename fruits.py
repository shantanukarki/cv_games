import cv2
import time
import random
import mediapipe as mp
import math
import json
import numpy as np



Lives = 15


def Spawn_Fruits(Fruits):
    fruit = {}
    random_x = random.randint(15, 600)
    random_color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    fruit["Color"] = random_color
    fruit["Curr_position"] = [random_x, 440]
    fruit["Next_position"] = [0, 0]
    Fruits.append(fruit)


def Fruit_Movement(Fruits, speed,img,Fruit_Size):
    global Lives

    for fruit in Fruits:
        if (fruit["Curr_position"][1]) < 20 or (fruit["Curr_position"][0]) > 650:
            Lives = Lives - 1
            Fruits.remove(fruit)

        cv2.circle(img, tuple(fruit["Curr_position"]), Fruit_Size, fruit["Color"], -1)
        fruit["Next_position"][0] = fruit["Curr_position"][0] + speed[0]
        fruit["Next_position"][1] = fruit["Curr_position"][1] - speed[1]

        fruit["Curr_position"] = fruit["Next_position"]


def distance(a, b):
    x1 = a[0]
    y1 = a[1]

    x2 = b[0]
    y2 = b[1]

    d = math.sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))
    return int(d)


def reset_game():
    global Score, Lives, Difficulty_level, game_Over, next_Time_to_Spawn, Fruits, slash

    Score = 0
    Lives = 15
    Difficulty_level = 1
    game_Over = False
    next_Time_to_Spawn = 0
    Fruits = []
    slash = np.array([[]], np.int32)


def close_game():
    global game_Over
    game_Over = True


def fruits():
    cap = cv2.VideoCapture(0)
    f = open("highscore.json")
    highscores = json.load(f)
    ninja_highscore = highscores['ninja']

    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles
    mp_hands = mp.solutions.hands

    hands = mp_hands.Hands(False, 1, 1, 0.5)

    curr_Frame = 0
    prev_Frame = 0
    delta_time = 0

    next_Time_to_Spawn = 0
    Speed = [0, 5]
    Fruit_Size = 30
    Spawn_Rate = 1
    Score = 0
    global Lives
    Difficulty_level = 1
    game_Over = False

    slash = np.array([[]], np.int32)
    slash_Color = (255, 255, 255)
    slash_length = 19

    w = h = 0

    Fruits = []
    while (cap.isOpened()):
        success, img = cap.read()
        if not success:
            continue
        h, w, c = img.shape
        img = cv2.cvtColor(cv2.flip(img, 1), cv2.COLOR_BGR2RGB)
        img.flags.writeable = False
        results = hands.process(img)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    img,
                    hand_landmarks,
                    mp_hands.HAND_CONNECTIONS,
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())

                for id, lm in enumerate(hand_landmarks.landmark):
                    if id == 8:
                        index_pos = (int(lm.x * w), int(lm.y * h))
                        cv2.circle(img, index_pos, 18, slash_Color, -1)
                        slash = np.append(slash, index_pos)

                        while len(slash) >= slash_length:
                            slash = np.delete(slash, len(slash) - slash_length, 0)

                        for fruit in Fruits:
                            d = distance(index_pos, fruit["Curr_position"])
                            cv2.putText(img, str(d), fruit["Curr_position"], cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 0), 2, 3)
                            if (d < Fruit_Size):
                                Score = Score + 100
                                slash_Color = fruit["Color"]
                                Fruits.remove(fruit)

        if Score % 1000 == 0 and Score != 0:
            Difficulty_level = (Score / 1000) + 1
            Difficulty_level = int(Difficulty_level)
            Spawn_Rate = Difficulty_level * 4 / 5
            Speed[0] = Speed[0] * Difficulty_level
            Speed[1] = int(5 * Difficulty_level / 2)

        if (Lives <= 0):
            game_Over = True

        slash = slash.reshape((-1, 1, 2))
        cv2.polylines(img, [slash], False, slash_Color, 15, 0)

        curr_Frame = time.time()
        delta_Time = curr_Frame - prev_Frame
        FPS = int(1 / delta_Time)
        cv2.putText(img, "FPS : " + str(FPS), (int(w * 0.82), 50), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 250, 0), 2)
        cv2.putText(img, "Score: " + str(Score), (int(w * 0.35), 90), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 5)
        cv2.putText(img, "Level: " + str(Difficulty_level), (int(w * 0.01), 90), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    (255, 0, 150), 5)
        cv2.putText(img, "Highscore : " + str(ninja_highscore), (int(w * 0.7), 90), cv2.FONT_HERSHEY_SIMPLEX, .5, (0, 0, 255), 2)
        cv2.putText(img, "Lives remaining : " + str(Lives), (200, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)

        prev_Frame = curr_Frame

        if not (game_Over):
            if (time.time() > next_Time_to_Spawn):
                Spawn_Fruits(Fruits)
                next_Time_to_Spawn = time.time() + (1 / Spawn_Rate)

            Fruit_Movement(Fruits, Speed,img,Fruit_Size)

        else:
            if(Score>ninja_highscore):
                highscores['ninja'] = Score
                with open('highscore.json', 'w') as file:
                    json.dump(highscores, file, indent=4)
                f = open("highscore.json")
                highscores = json.load(f)
                ninja_highscore = highscores['ninja']
                cv2.putText(img, "New High Score", (int(w * 0.1), int(h * 0.6)), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 2)
            else:
                cv2.putText(img, "GAME OVER", (int(w * 0.1), int(h * 0.6)), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 3)
            Fruits.clear()

        cv2.imshow("img", img)

        key = cv2.waitKey(5)
        if key & 0xFF == 27:  # Exit key (Esc)
            break
        elif key == ord('r'):  # Reset key ('r')
            reset_game()

        # Check if the user clicks the close button in the top bar
        if cv2.getWindowProperty("img", cv2.WND_PROP_VISIBLE) < 1:
            break

    cap.release()
    cv2.destroyAllWindows()