The project is created with the following libraries:
cvzone                    1.6.1
pygame                    2.6.0
numpy                     2.0.0
opencv-python             4.10.0.84
mediapipe                 0.10.14

These are the required libraries that must be installed before running the code. They can be installer with pip with the commands
pip install cvzone==1.6.1
pip install pygame==2.6.0
pip install numpy==2.0.0
pip install opencv-python==4.10.0.84
pip install mediapipe==0.10.14

After installing the given libraries run the main.py file to get the game. You can run the file with the command
python main.py