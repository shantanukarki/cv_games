import sys
import time
import random
import pygame
from collections import deque
import cv2 as cv
import mediapipe as mp
import json


def reset_game(game_clock, stage, pipeSpawnTimer, time_between_pipe_spawn, dist_between_pipes, level, score, didUpdateScore, game_is_running, pipe_frames,bird_frame,window_size):
    game_clock = time.time()
    stage = 1
    pipeSpawnTimer = 0
    time_between_pipe_spawn = 40
    dist_between_pipes = 500
    level = 0
    score = 0
    didUpdateScore = False
    game_is_running = True
    pipe_frames.clear()
    bird_frame.center = (window_size[0] // 6, window_size[1] // 2)
    return game_clock, stage, pipeSpawnTimer, time_between_pipe_spawn, dist_between_pipes, level, score, didUpdateScore, game_is_running, pipe_frames

def calculate_bird_speed(marker_y,base_scaling_factor,stage,speed_increase_per_stage,window_size):
    # Calculate scaling factor based on current stage
    scaling_factor = base_scaling_factor + (stage - 1) * speed_increase_per_stage
    return int((marker_y - 0.5) * scaling_factor * window_size[1] + window_size[1] / 2)

def flappy():
    f = open("highscore.json")
    highscores = json.load(f)
    bird_highscore = highscores['bird']

    mp_drawing = mp.solutions.drawing_utils
    mp_face_mesh = mp.solutions.face_mesh
    drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)

    pygame.init()

    VID_CAP = cv.VideoCapture(0)
    VID_CAP.set(cv.CAP_PROP_FRAME_WIDTH, 1280)
    VID_CAP.set(cv.CAP_PROP_FRAME_HEIGHT, 720)
    window_size = (1280, 720)
    screen = pygame.display.set_mode(window_size)
    pygame.display.set_caption('Flappy Bird CV')
    

    # Load bird and pipe images
    bird_img = pygame.image.load("sprites/bird_sprite.png")
    bird_img = pygame.transform.scale(bird_img, (bird_img.get_width() // 6, bird_img.get_height() // 6))
    bird_frame = bird_img.get_rect()
    bird_frame.center = (window_size[0] // 6, window_size[1] // 2)
    pipe_img = pygame.image.load("sprites/pipe_sprite_single.png")

    # Initialize pipe settings
    pipe_frames = deque()
    pipe_starting_template = pipe_img.get_rect()
    space_between_pipes = 250

    # Game settings
    game_clock = time.time()
    stage = 1
    pipeSpawnTimer = 0
    time_between_pipe_spawn = 40
    dist_between_pipes = 500
    pipe_velocity = lambda: dist_between_pipes / time_between_pipe_spawn
    level = 0
    score = 0
    didUpdateScore = False
    game_is_running = True

    # Define base scaling factor and speed increase per stage level
    base_scaling_factor = 1.5
    speed_increase_per_stage = 0.1
    with mp_face_mesh.FaceMesh(
            max_num_faces=1,
            refine_landmarks=True,
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as face_mesh:
        
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    VID_CAP.release()
                    cv.destroyAllWindows()
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN and not game_is_running:
                    if event.key == pygame.K_r:
                        game_clock, stage, pipeSpawnTimer, time_between_pipe_spawn, dist_between_pipes, level, score, didUpdateScore, game_is_running, pipe_frames = reset_game(game_clock, stage, pipeSpawnTimer, time_between_pipe_spawn, dist_between_pipes, level, score, didUpdateScore, game_is_running, pipe_frames,bird_frame,window_size)

            ret, frame = VID_CAP.read()
            if not ret:
                continue

            screen.fill((125, 220, 232))
            
            frame.flags.writeable = False
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
            results = face_mesh.process(frame)
            frame.flags.writeable = True

            if results.multi_face_landmarks:
                marker = results.multi_face_landmarks[0].landmark[94].y
                bird_frame.centery = calculate_bird_speed(marker,base_scaling_factor,stage,speed_increase_per_stage,window_size)
                if bird_frame.top < 0:
                    bird_frame.top = 0
                if bird_frame.bottom > window_size[1]:
                    bird_frame.bottom = window_size[1]

            frame = cv.flip(frame, 1).swapaxes(0, 1)

            for pf in pipe_frames:
                pf[0].x -= pipe_velocity()
                pf[1].x -= pipe_velocity()

            if pipe_frames and pipe_frames[0][0].right < 0:
                pipe_frames.popleft()

            pygame.surfarray.blit_array(screen, frame)
            screen.blit(bird_img, bird_frame)

            checker = True
            for pf in pipe_frames:
                if pf[0].left <= bird_frame.x <= pf[0].right:
                    checker = False
                    if not didUpdateScore:
                        score += 1
                        didUpdateScore = True
                screen.blit(pipe_img, pf[1])
                screen.blit(pygame.transform.flip(pipe_img, 0, 1), pf[0])
            if checker:
                didUpdateScore = False

            stage_text = pygame.font.SysFont("Helvetica Bold.ttf", 50).render(f'Stage {stage}', True, (99, 245, 255))
            score_text = pygame.font.SysFont("Helvetica Bold.ttf", 50).render(f'Score: {score}', True, (99, 245, 255))
            high_score_text = pygame.font.SysFont("Helvetica Bold.ttf", 50).render(f'High Score: {bird_highscore}', True, (99, 245, 255))
            screen.blit(stage_text, (100, 50))
            screen.blit(score_text, (100, 100))
            screen.blit(high_score_text, (100, 150))

            pygame.display.flip()

            if game_is_running:
                if any(bird_frame.colliderect(pf[0]) or bird_frame.colliderect(pf[1]) for pf in pipe_frames):
                    game_is_running = False
                if pipeSpawnTimer == 0:
                    top = pipe_starting_template.copy()
                    top.x = window_size[0]
                    top.y = random.randint(120 - 1000, int(window_size[1] - 120 - space_between_pipes - 1000))
                    bottom = pipe_starting_template.copy()
                    bottom.x = window_size[0]
                    bottom.y = top.y + 1000 + space_between_pipes
                    pipe_frames.append([top, bottom])
                pipeSpawnTimer = (pipeSpawnTimer + 1) % time_between_pipe_spawn
                if time.time() - game_clock >= 10:  
                    time_between_pipe_spawn = max(20, time_between_pipe_spawn * 5 // 6)
                    stage += 1
                    game_clock = time.time()
            else:
                if score >= bird_highscore:
                    highscores['bird'] = score
                    bird_highscore = score
                    with open('highscore.json', 'w') as file:
                        json.dump(highscores, file, indent=4)
                    
                    high_text = pygame.font.SysFont("Helvetica Bold.ttf", 64).render('New Highscore! Press R to restart', True, (99, 245, 255))
                    tr = high_text.get_rect()
                    tr.center = (window_size[0] / 2, window_size[1] / 2)
                    screen.blit(high_text, tr)
                    pygame.display.update()
                    pygame.time.wait(100)
                else:
                    text = pygame.font.SysFont("Helvetica Bold.ttf", 64).render('Game over! Press R to restart', True, (99, 245, 255))
                    tr = text.get_rect()
                    tr.center = (window_size[0] / 2, window_size[1] / 2)
                    screen.blit(text, tr)
                    pygame.display.update()
                    pygame.time.wait(100)
