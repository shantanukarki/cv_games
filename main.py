import tkinter as tk
from snake import snake
from fruits import fruits
from flappy import  flappy

def f1():
    snake()

def f2():
    fruits()

def f3():
    flappy()

def option_selected(option):
    if option == "Snake Game":
        f1()
    elif option == "Fruit Ninja":
        f2()
    elif option == "Flappy Bird":
        f3()

root = tk.Tk()
root.title("Choose an Option")

def create_option_button(option):
    return tk.Button(root, text=f"{option}", command=lambda: option_selected(option))

button1 = create_option_button("Snake Game")
button1.pack(pady=10)

button2 = create_option_button("Fruit Ninja")
button2.pack(pady=10)

button2 = create_option_button("Flappy Bird")
button2.pack(pady=10)

# Get the screen width and height
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

x = (screen_width // 2) - (root.winfo_reqwidth() // 2)
y = (screen_height // 2) - (root.winfo_reqheight() // 2)

root.geometry(f"+{x}+{y}")

root.mainloop()
