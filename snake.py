import math
import random
import cvzone
import cv2
import json
import numpy as np
from cvzone.HandTrackingModule import HandDetector





def random_food_location():
    return random.randint(100, 1000), random.randint(100, 600)

def update(imgMain, currentHead, points, lengths, currentLength, allowedLength, previousHead, foodPoint, imgFood, score, gameOver,snake_highscore,highscores):
    
    if gameOver:
        cvzone.putTextRect(imgMain, "Game Over", [300, 300],
                        scale=7, thickness=5, offset=20)
        if score > snake_highscore:
            highscores['snake'] = score
            snake_highscore = score
            with open('highscore.json', 'w') as file:
                json.dump(highscores, file, indent=4)
            

            cvzone.putTextRect(imgMain, f'Confratulations !!!', [100, 450],
                        scale=7, thickness=5, offset=20)
            cvzone.putTextRect(imgMain, f'New Highscore: {score}', [200, 600],
                        scale=7, thickness=5, offset=20)
            return imgMain, points, lengths, currentLength, allowedLength, previousHead, foodPoint, score, gameOver,snake_highscore
        cvzone.putTextRect(imgMain, f'Your Score: {score}', [300, 450],
                        scale=7, thickness=5, offset=20)
        cvzone.putTextRect(imgMain, f'Highscore: {snake_highscore}', [300, 600],
                        scale=7, thickness=5, offset=20)
        return imgMain, points, lengths, currentLength, allowedLength, previousHead, foodPoint, score, gameOver,snake_highscore
    else:
        px, py = previousHead
        cx, cy = currentHead

        points.append([cx, cy])
        distance = math.hypot(cx - px, cy - py)
        lengths.append(distance)
        currentLength += distance
        previousHead = cx, cy

        if currentLength > allowedLength:
            for i, length in enumerate(lengths):
                currentLength -= length
                lengths.pop(i)
                points.pop(i)
                if currentLength < allowedLength:
                    break

        # Check if snake ate the Food
        rx, ry = foodPoint
        hFood, wFood, _ = imgFood.shape
        if rx - wFood // 2 < cx < rx + wFood // 2 and \
                ry - hFood // 2 < cy < ry + hFood // 2:
            foodPoint = random_food_location()
            allowedLength += 50
            score += 1

        # Draw Snake
        if points:
            for i, point in enumerate(points):
                if i != 0:
                    cv2.line(imgMain, points[i - 1], points[i], (0, 0, 255), 20)
            cv2.circle(imgMain, points[-1], 20, (0, 255, 0), cv2.FILLED)

        # Draw Food
        imgMain = cvzone.overlayPNG(imgMain, imgFood,
                                    (rx - wFood // 2, ry - hFood // 2))

        cvzone.putTextRect(imgMain, f'Score: {score}', [50, 80],
                        scale=3, thickness=3, offset=10)

        # Check for Collision
        pts = np.array(points[:-2], np.int32)
        pts = pts.reshape((-1, 1, 2))
        cv2.polylines(imgMain, [pts], False, (0, 255, 0), 3)
        minDist = cv2.pointPolygonTest(pts, (cx, cy), True)

        if -1 <= minDist <= 1:
            gameOver = True
            points = []  
            lengths = []  
            currentLength = 0  
            allowedLength = 150  
            previousHead = 0, 0  
            foodPoint = random_food_location()

    return imgMain, points, lengths, currentLength, allowedLength, previousHead, foodPoint, score, gameOver,snake_highscore


# Initialization
# cap = cv2.VideoCapture(0)
# cap.set(3, 1280)
# cap.set(4, 720)
# detector = HandDetector(maxHands=1, detectionCon=0.8)

# imgFood = cv2.imread("sprites/cookie.png", cv2.IMREAD_UNCHANGED)
# foodPoint = random_food_location()

# points = []  
# lengths = []  
# currentLength = 0  
# allowedLength = 150  
# previousHead = 0, 0  
# score = 0
# gameOver = False

def snake():
    f = open("highscore.json")
    highscores = json.load(f)
    snake_highscore = highscores['snake']
    cap = cv2.VideoCapture(0)
    cap.set(3, 1280)
    cap.set(4, 720)
    detector = HandDetector(maxHands=1, detectionCon=0.8)

    imgFood = cv2.imread("sprites/cookie.png", cv2.IMREAD_UNCHANGED)
    foodPoint = random_food_location()

    points = []  
    lengths = []  
    currentLength = 0  
    allowedLength = 150  
    previousHead = 0, 0  
    score = 0
    gameOver = False
    while True:
        success, img = cap.read()
        img = cv2.flip(img, 1)
        hands, img = detector.findHands(img, flipType=False)

        if hands:
            lmList = hands[0]['lmList']
            pointIndex = lmList[8][0:2]
            img, points, lengths, currentLength, allowedLength, previousHead, foodPoint, score, gameOver,snake_highscore = update(
                img, pointIndex, points, lengths, currentLength, allowedLength, previousHead, foodPoint, imgFood, score, gameOver,snake_highscore,highscores
            )
        cv2.imshow("Image", img)
        key = cv2.waitKey(1)
        
        # Check if window is closed
        if cv2.getWindowProperty("Image", cv2.WND_PROP_VISIBLE) < 1:
            break
        
        # Restart game
        if key == ord('r'):
            gameOver = False
            score = 0
            cvzone.putTextRect(img, "Game Restarted", [300, 400],
                            scale=7, thickness=5, offset=20)
        
        # Exit on ESC key
        if key == 27:
            break

    # Release resources
    cap.release()
    cv2.destroyAllWindows()
    # sys.exit()
